﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using VRTK;

public class CanvasEvents : MonoBehaviour
{
    public GameObject menu;
    public GameObject mainMenu;
    public GameObject infoPanel;
    public GameObject credit;
    public GameObject quitObject;

    public Animator moveToTowerAnim;
    public Animator scaleMenuAnim;
    public Animator creditAnimator;

    private Coroutine creditCoroutine;

    public bool menuActive = false;
    public bool mainMenuActive = false;
    public bool creditActive = false;
    public bool quitterActive = false;
    
    private VRTK_UICanvas vrtkUiCanvas;

    private void Start()
    {
        vrtkUiCanvas = GetComponent<VRTK_UICanvas>();
        SetUiCanvas();
        if (quitterActive)
            StartCoroutine(QuitterWait());
    }


    public void ChangeScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void ChangeSceneWithFade(string sceneToLoad)
    {
        StartCoroutine(ChangeSceneFade(sceneToLoad));
    }

    public void Resume()
    {
        menuActive = false;
        menu.SetActive(false);
        SetUiCanvas();
    }

    public void OpenMenu()
    {
        menuActive = true;
        menu.SetActive(true);
        SetUiCanvas();
    }

    public void MoveToTower()
    {
        StartCoroutine(MoveTowardTower());
    }

    public void CloseInfoPanel()
    {
        infoPanel.SetActive(false);
    }

    public void CloseMainMenu()
    {
        mainMenuActive = false;
        mainMenu.SetActive(false);
        SetUiCanvas();
    }

    public void OpenMainMenu()
    {
        mainMenuActive = true;
        mainMenu.SetActive(false);
        SetUiCanvas();
    }

    public void OpenCredit()
    {
        creditActive = true;
        mainMenuActive = true;
        mainMenu.SetActive(false);
        SetUiCanvas();
        credit.SetActive(true);
        Debug.Log("sa marche ou pas");
        creditAnimator.Play("credits");
        Debug.Log("Ou alors la sa marche peut être");
        creditCoroutine = StartCoroutine(CreditWait());
    }

    public void CloseCredit()
    {
        creditActive = false;
        credit.SetActive(false);
        mainMenu.SetActive(true);
        mainMenuActive = true;
        StopCoroutine(creditCoroutine);
    }
    
    private void SetUiCanvas()
    {
        vrtkUiCanvas.enabled = mainMenuActive || menuActive;
    }

    public IEnumerator MoveTowardTower()
    {
        scaleMenuAnim.SetBool("scale", true);
        moveToTowerAnim.enabled = true;
        yield return new WaitForSeconds(3f);
        FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", true);
        while (!FadeBlackDetector.instance.isBlack)
        {
            yield return null;
        }
        ChangeScene("MainScene");
    }
    
    public IEnumerator ChangeSceneFade(string sceneName)
    {
        FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", true);
        while (!FadeBlackDetector.instance.isBlack)
        {
            yield return null;
        }
        ChangeScene(sceneName);
    }

    public IEnumerator CreditWait()
    {
        yield return new WaitForSeconds(50f);
        CloseCredit();
    }

    public IEnumerator QuitterWait()
    {
        yield return new WaitForSeconds(15f);
        quitObject.SetActive(true);
    }
}
