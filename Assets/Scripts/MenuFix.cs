﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using VRTK;

public class MenuFix : MonoBehaviour
{
    public GameObject EventSystem;
    public VRTK_UIPointer PointerControllerRight;
    public VRTK_UIPointer PointerControllerLeft;
 
    private VRTK_VRInputModule[] inputModule;

    public bool setup = false;
 
    private void Start()
    {
        SetupControllers();
    }
 
    private void Update()
    {
        if (inputModule != null && EventSystem.active && !EventSystem.GetComponent<EventSystem>().enabled)
        {
            if (inputModule.Length > 0)
                if (inputModule[0].pointers.Count < 2 && !setup)
                {
                    setup = true;
                    inputModule[0].enabled = true;
                    if (inputModule[0].pointers.Count == 0 || inputModule[0].pointers.Count == 1)
                    {
                        inputModule[0].pointers = new List<VRTK_UIPointer>();
                        
                        inputModule[0].pointers.Add(PointerControllerRight);
                        inputModule[0].pointers.Add(PointerControllerLeft);
                    }

                    for (int i = 0; i < inputModule.Length; i++)
                    {
                        if (i > 0)
                        {
                            Destroy(inputModule[i]);
                        }
                    }
                }
                else
                    inputModule = EventSystem.GetComponents<VRTK_VRInputModule>();
        }
        else
        {
            SetupControllers();
        }
    }

    void SetupControllers()
    {
        setup = false;
        EventSystem.SetActive(true);
        EventSystem.GetComponent<EventSystem>().enabled = false;
        inputModule = EventSystem.GetComponents<VRTK_VRInputModule>();
    }
}
