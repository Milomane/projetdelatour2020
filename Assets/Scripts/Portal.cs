﻿ using System;
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using UnityEngine.SceneManagement;

 public class Portal : MonoBehaviour
 {
     public Transform targetPosition;

     private float portalDistance =1f;
     // Update is called once per frame
    void Update()
    {
        transform.LookAt(targetPosition);
        Vector3 eulerAngles = transform.rotation.eulerAngles;
        eulerAngles.x = 0;
        eulerAngles.z = 0;

        // Set the altered rotation back
        transform.rotation = Quaternion.Euler(eulerAngles);
        
        VerifPortalHit();
    }

    void VerifPortalHit()
    {
        float distance = Vector3.Distance(targetPosition.position, transform.position);
        if (distance < 1)
        {
            StartCoroutine(AnimToChangeScene());
        }
    }

    private IEnumerator AnimToChangeScene()
    {
        FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", true);
        while (!FadeBlackDetector.instance.isBlack)
        {
            yield return null;
        }
        SceneManager.LoadScene("VirtualVisit");
    }
 }
