using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class Assemblescript : MonoBehaviour
{
	[SerializeField] private GameObject[] parts;
	[SerializeField] private GameObject finalobject;
	[SerializeField] private GameObject portal;
	[SerializeField] private float radius;

	private Collider[] collide;

	[SerializeField] private int[] dialogindex;
	[SerializeField] private bool[] haveaudio;


	void Update(){
		
		collide = Physics.OverlapSphere(transform.position, radius);

		if(assemblecondition()){
			Debug.Log("WORKING");

			VictorySound.instance.StartPlaySound();
			finalobject.SetActive(true);
			portal.SetActive(true);
			finalobject.transform.position = transform.position;
			for(int i = 0; i < parts.Length; i++){
				Destroy(parts[i]);
			}
			for (int i = 0; i < dialogindex.Length; i++)
			{
				Subtitles.Instance.Play(dialogindex[i], haveaudio[i]);
			}
			gameObject.SetActive(false);
		}
	}
	
	bool assemblecondition(){
		
		for(int i = 0; i < parts.Length; i++){
			if (Vector3.Distance(parts[i].transform.position, transform.position) > radius)
			{
				return false;
			}
		}
		return true;
	}
	
	
	
	
	
}