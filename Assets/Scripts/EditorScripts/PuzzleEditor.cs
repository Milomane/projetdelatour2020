﻿#if (UNITY_EDITOR)


using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Puzzle))]
public class PuzzleEditor : Editor
{
    private Puzzle puzzlescript;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        puzzlescript = (Puzzle) target;

        if (GUILayout.Button("Set Puzzle"))
        {
            puzzlescript.InitPuzzle();
        }


    }


}
#endif