﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LoadSondObject : MonoBehaviour
{
    public AudioSource sound;
    private bool launch = false;
    public float timer = 5;

    public float minPitch = .2f;
    public float minVol = .5f;
    
    void Update()
    {
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
            launch = true;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (launch)
        {
            sound.pitch = Random.Range(minPitch, 1f);
            sound.volume = Random.Range(minVol, 1f);
            sound.Play();
            timer = 1;
            launch = false;
            Debug.Log(gameObject.name);
        }
    }
}
