﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonActuel : MonoBehaviour
{
    public List<AudioClip> testSound = new List<AudioClip>();
    public AudioSource olivier; 
    [SerializeField]  private bool end;
    [SerializeField] private float timer = 2;
    public int numberSound;
    // Start is called before the first frame update
    void Start()
    {
        olivier.clip = testSound[0];
        olivier.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (olivier.isPlaying == false)
        {
            Timer();
        }
        
        if (end /* && olivier.isPlaying == false*/) 
        {
            numberSound++; 
            olivier.clip = testSound[numberSound]; 
            olivier.Play();
            end = false;
            timer = 2;
        }

      
    }

    void Timer()
    {

        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }
        if (timer <= 0)
        {
            end = true;
        }
    }
}
