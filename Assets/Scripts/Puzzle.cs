using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using VRTK;

public class Puzzle : MonoBehaviour
{
    [SerializeField] private GameObject[] parts;
    [SerializeField] private GameObject completedpuzzle;
    //[SerializeField] private int linesize;
    private Collider[][] zonecollider;
    [SerializeField] private Vector3[] colliderposition;
    [SerializeField] private float radius;
    [SerializeField] private Rigidbody objecttodrop;
    [SerializeField] private int[] dialogindex;
    [SerializeField] private bool[] haveaudio;



    private string arraysort;

    private int index = 1;
    
    void Start()
    {
        zonecollider = new Collider[parts.Length][];
    }
    void Update()
    {
        
        for (int i = 0; i < zonecollider.Length; i++)
        {
            arraysort = "";
            zonecollider[i] = Physics.OverlapSphere(colliderposition[i], radius);
            for (int j = 0; j < zonecollider[i].Length; j++)
            {
                arraysort += zonecollider[i][j].name + " : NEXT : ";
            }
            
        }

        if (Checkpuzzle(zonecollider, parts))
        {
            for (int i = 0; i < parts.Length; i++)
            {
                Destroy(parts[i]);
            }
            
            VictorySound.instance.StartPlaySound();

            completedpuzzle.SetActive(true);
            objecttodrop.constraints = RigidbodyConstraints.None;
            objecttodrop.useGravity = true;
            for (int i = 0; i < dialogindex.Length; i++)
            {
                Subtitles.Instance.Play(dialogindex[i], haveaudio[i]);
            }
            Destroy(this);
        }
        else
        {
        }





    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < colliderposition.Length; i++)
        {
            Gizmos.DrawSphere(colliderposition[i], radius);
        }
    }
    
    


    public void InitPuzzle()
    {
        Debug.Log("INIT PUZZLE");
        colliderposition = new Vector3[parts.Length];
        for (int i = 0; i < colliderposition.Length; i++)
        {
            colliderposition[i] = parts[i].transform.position;
        }
        
    }

    bool Checkpuzzle(Collider[][] colliders, GameObject[] checkedparts)
    {
        for (int i = 0; i < colliders.Length; i++)
        {
            if (!convertCollidersToGameObjects(colliders[i]).Contains(checkedparts[i])) return false;
        }

        return true;
    }
    
    GameObject[] convertCollidersToGameObjects(Collider[] colliders)
    {
        GameObject[] collideobjects = new GameObject[colliders.Length];
        for (int i = 0; i < colliders.Length; i++)
        {
            collideobjects[i] = colliders[i].gameObject;
        }

        return collideobjects;
    }

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < parts.Length; i++)
        {
            if (other.gameObject == parts[i])
            {
                other.GetComponent<Rigidbody>().isKinematic = true;
                other.GetComponent<AudioSource>().enabled = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        for (int i = 0; i < parts.Length; i++)
        {
            if (other.gameObject == parts[i])
            {
                StartCoroutine(waitforactivation(other.gameObject));
                other.GetComponent<AudioSource>().enabled = true;
                //other.GetComponent<Rigidbody>().isKinematic = false;
            }
        }
    }

    IEnumerator waitforactivation(GameObject target)
    {
        while (target.GetComponent<VRTK_InteractableObject>().enabled)
        {
            yield return new WaitForEndOfFrame();
        }

        target.GetComponent<Rigidbody>().isKinematic = false;
        yield return null;
    }
}