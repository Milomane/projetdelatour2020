﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ControllerEventInteraction : MonoBehaviour
{
    private VRTK_ControllerEvents controllerEvents;
    
    private void OnEnable()
    {
        controllerEvents = GetComponent<VRTK_ControllerEvents>();
        if (controllerEvents == null)
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(
                VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT,
                "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
            return;
        }
        
        controllerEvents.StartMenuReleased += ControllerEventsOnStartMenuReleased;
    }

    private void ControllerEventsOnStartMenuReleased(object sender, ControllerInteractionEventArgs e)
    {
        FindObjectOfType<CanvasEvents>().OpenMenu();
    }
}
