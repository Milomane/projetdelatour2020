﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Subtitles : MonoBehaviour
{
    public static Subtitles Instance;
    [SerializeField] private SonPasse audioscript;
    [SerializeField] private List<string> subtitles = new List<string>();
    [SerializeField] private TextMeshProUGUI subtext;
    [SerializeField] private GameObject subpanel;
    [SerializeField] private float speed;
    [SerializeField] private float endtime;
    private Queue<int> soundqueue = new Queue<int>();
    private Queue<bool> havesoundqueue = new Queue<bool>();
    public bool subActive = false;
    


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        if (audioscript == null) this.enabled = false;
        //Play(1);

    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P)) Play(1,true);

        if (soundqueue.Count > 0 && audioscript.endSound)
        {
            Play(soundqueue.Dequeue(), havesoundqueue.Dequeue());
        }
    }

    public void Play(int audioindex, bool haveaudio)
    {
        if (audioscript.endSound)
        {
            if (haveaudio) audioscript.Phase(audioindex);
            StartCoroutine(playsub(audioindex, haveaudio));
        }
        else
        {
            soundqueue.Enqueue(audioindex);
            havesoundqueue.Enqueue(haveaudio);
        }

    }

    IEnumerator playsub(int textindex, bool haveaudio)
    {
        subActive = true;
        audioscript.endSound = false;
        string currenttext = "";
        subpanel.SetActive(true);

        for (int i = 0; i < subtitles[textindex].Length; i++)
        {
            currenttext += subtitles[textindex][i];
            subtext.text = currenttext;
            if (haveaudio)
            {
                yield return new WaitForSeconds(audioscript.soundManager.clip.length / subtitles[textindex].Length * speed);
            }
            else
            {
                yield return new WaitForSeconds(0.05f);
            }

        }

        while (audioscript.soundManager.isPlaying)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(endtime);
        subtext.text = "";
        subpanel.SetActive(false);
        subActive = false;
        audioscript.endSound = true;
        yield return null;
    }
    
}
