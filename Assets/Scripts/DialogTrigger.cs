﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject target_to_detect;
    [SerializeField] private float distance;
    [SerializeField] private int[] dialogindex;
    [SerializeField] private bool[] dialogaudio;

    private void Update()
    {
        if (Vector3.Distance(transform.position, target_to_detect.transform.position) <= distance)
        {
            for (int i = 0; i < dialogindex.Length; i++)
            {
                Subtitles.Instance.Play(dialogindex[i], dialogaudio[i]);
            }
            
            enabled = false;
        }
    }

    private void OnDrawGizmos()
    {
        //Gizmos.DrawSphere(transform.position, distance);
    }
}
