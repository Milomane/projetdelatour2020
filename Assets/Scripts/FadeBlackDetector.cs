﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeBlackDetector : MonoBehaviour
{
    public static FadeBlackDetector instance;
    public bool isBlack = true;

    void Start()
    {
        instance = this;
    }
    
    public void SetBlack()
    {
        isBlack = true;
        Debug.Log("isBlack");
    }
    
    public void SetNotBlack()
    {
        isBlack = false;
        Debug.Log("isNotBlack");
    }
}
