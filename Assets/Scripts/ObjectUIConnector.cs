﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ObjectUIConnector : MonoBehaviour
{
	[SerializeField] private string name;
	[SerializeField] private string description;
	private bool spawned = false; 

    private void Update()
    {
		if(GetComponent<VRTK_InteractableObject>().enabled && !Subtitles.Instance.subActive){
			if(Items.Instance.newobjectdescribe(name, description)){
				spawned = true;
				Debug.Log("UI description Applied");
			}else{
				Debug.Log("UI description can't be applied");
			}
		}else if (spawned){
			Items.Instance.disableui();
			spawned = false;

		}
    }
}
