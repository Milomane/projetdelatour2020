﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Items : MonoBehaviour
{
	[SerializeField] private GameObject uipannel;
	[SerializeField] private TMP_Text nametext;
	[SerializeField] private TMP_Text descriptiontext;
	public static Items Instance;

    private void Awake()
    {
		if(Instance != null){
			Destroy(this);
		}else{
			Instance = this;
		}

    }
	
	public bool newobjectdescribe(string name, string description){
		uipannel.SetActive(true);
		nametext.text = name;
		descriptiontext.text = description;
		return true;
		
	}
	
	public void disableui(){
		uipannel.SetActive(false);
	}
	
    
}
