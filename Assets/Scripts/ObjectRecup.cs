﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ObjectRecup : MonoBehaviour
{
    public bool isGrab;

    public float timer = 0.1f;

    public bool isInventory;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.parent != null)
        {
            isGrab = true;
        }

        if (isGrab)
        {
            isInventory = true;
            if (timer >= 0)
            {
                timer -= Time.deltaTime;
            }

            if (timer <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }

   
}
