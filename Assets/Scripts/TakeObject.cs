﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TakeObject : MonoBehaviour
{
    void Start()
    {
        if (GetComponent<VRTK_ControllerEvents>() != null)
        {
            GetComponent<VRTK_ControllerEvents>().TriggerReleased += new ControllerInteractionEventHandler(DoTriggerReleased);
        }
    }

    void DoTriggerReleased(object sender, ControllerInteractionEventArgs e)
    {
        if (GetComponent<VRTK_InteractTouch>() != null)
        {
            
        }
    }
    
    
    void Update()
    {
        
    }
}
