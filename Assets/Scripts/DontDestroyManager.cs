﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyManager : MonoBehaviour
{
    public static DontDestroyManager instance;

    public CanvasEvents canvasEvents;
    
    
    void Start()
    {
        StartCoroutine(Wait());
    }

    public void CloseAllUI()
    {
        canvasEvents.CloseInfoPanel();
        canvasEvents.Resume();
        canvasEvents.CloseMainMenu();
    }

    public void OpenMainMenu()
    {
        canvasEvents.OpenMainMenu();
    }

    void Update()
    {
        
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
