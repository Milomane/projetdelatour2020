﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    private bool unlocked = false;
    [SerializeField] private GameObject key;
    [SerializeField] private Animator chestanimator;
    [SerializeField] private int[] dialogindex;
    [SerializeField] private bool[] dialogaudio;
    private AudioSource audiosource;

    private void Awake()
    {
        audiosource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!unlocked && other.gameObject == key)
        {
            VictorySound.instance.StartPlaySound();
            chestanimator.enabled = true;
            audiosource.Play();
            Destroy(key);
            unlocked = true;
            Items.Instance.disableui();
            for (int i = 0; i < dialogindex.Length; i++)
            {
                Subtitles.Instance.Play(dialogindex[i], dialogaudio[i]);
            }
            
        }
    }
}
