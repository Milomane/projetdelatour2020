﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class RightHandEvent : MonoBehaviour
{
    private VRTK_ControllerEvents controllerEvents;

    public CanvasEvents canvasEvents;

    public bool buttonTwoInput = false;

    private void OnEnable()
    {
        controllerEvents = GetComponent<VRTK_ControllerEvents>();
        if (controllerEvents == null)
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(
                VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT,
                "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
            return;
        }
        
        controllerEvents.ButtonTwoReleased += DoButtonTwoReleased;
    }
    
    private void DebugLogger(uint index, string button, string action, ControllerInteractionEventArgs e)
    {
        string debugString = "Controller on index '" + index + "' " + button + " has been " + action
                             + " with a pressure of " + e.buttonPressure + " / Primary Touchpad axis at: " + e.touchpadAxis + " (" + e.touchpadAngle + " degrees)" + " / Secondary Touchpad axis at: " + e.touchpadTwoAxis + " (" + e.touchpadTwoAngle + " degrees)";
        VRTK_Logger.Info(debugString);
    }
    
    private void DoButtonTwoReleased(object sender, ControllerInteractionEventArgs e)
    {
        if (canvasEvents.creditActive)
        {
            canvasEvents.CloseCredit();
        }
        
        if (LevelManager.instance.canActivePause)
        {
            if (!buttonTwoInput)
            {
                buttonTwoInput = true;
                StartCoroutine(TimeBeforeEnableButtonTwo());
            
                if (canvasEvents.menuActive)
                {
                    canvasEvents.Resume();
                    Debug.Log("CloseMenu");
                }
                else
                {
                    canvasEvents.OpenMenu();
                    Debug.Log("OpenMenu");
                }
        
                DebugLogger(VRTK_ControllerReference.GetRealIndex(e.controllerReference), "BUTTON TWO", "released", e);
            }
        }
    }


    IEnumerator TimeBeforeEnableButtonTwo()
    {
        yield return new WaitForSeconds(.05f);
        buttonTwoInput = false;
    }

    private void Update()
    {
        
    }
}
