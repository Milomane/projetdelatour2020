﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Porte : MonoBehaviour
{
    private Animator _animator;
    public float timer;
    public bool isDoorClosed;

    public bool timerEnd;

    public GameObject chain;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (chain == null)
        {
            _animator.SetBool("isOpen", true);
            timer = 1;
            timerEnd = true;
         
        }
        
    } 
}

