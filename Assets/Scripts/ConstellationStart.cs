﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstellationStart : MonoBehaviour
{
    public float distanceToTrigger;
    public Transform headTransform;
    public Transform tpTransform;
    public Transform baseTransform;

    public Transform player;

    public Animator openBoxAnimator;
    public AudioSource sound;
    public AudioSource boxSound;

    [SerializeField] private int[] dialogindex;
    [SerializeField] private bool[] haveaudio;
    private bool done;
    private bool teleported;

    void Start()
    {
        
    }

    void Update()
    {
        if (!teleported)
            if (Vector3.Distance(headTransform.position, transform.position) < distanceToTrigger)
            {
                teleported = true;
                MusicManager.instance.ChangeMusic(0);
                sound.Play();
                StartCoroutine(TeleportToSpace());
            }
    }

    public void ConstBelierPressed()
    {
        // Bonne réponse
        StartCoroutine(TeleportBack());
    }
    
    public void ConstCapricornePressed()
    {
        // Play bad sound
    }
    
    public void ConstDragonPressed()
    {
        // Play bad sound
    }
    
    public void ConstScorpionPressed()
    {
        // Play bad sound
    }
    
    public void ConstGemeauxPressed()
    {
        // Play bad sound
    }

    public IEnumerator TeleportToSpace()
    {
        FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", true);
        yield return new WaitForSeconds(.1f);
        FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", false);
        while (!FadeBlackDetector.instance.isBlack)
        {
            yield return null;
        }
        player.transform.position = tpTransform.position;
    }
    
    public IEnumerator TeleportBack()
    {
        if (!done)
        {
            done = true;
            MusicManager.instance.ChangeMusic(1);
            VictorySound.instance.StartPlaySound();
            boxSound.Play();
            FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", true);
            yield return new WaitForSeconds(.1f);
            FadeBlackDetector.instance.GetComponent<Animator>().SetBool("Fade", false);
            while (!FadeBlackDetector.instance.isBlack)
            {
                yield return null;
            }
            
            openBoxAnimator.SetBool("open", true);
            
            player.transform.position = baseTransform.position;
            for (int i = 0; i < dialogindex.Length; i++)
            {
                Subtitles.Instance.Play(dialogindex[i], haveaudio[i]);
            }
        }
        
        
        
    }
}
