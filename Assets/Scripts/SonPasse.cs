﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonPasse : MonoBehaviour
{
    public List<AudioClip> voice = new List<AudioClip>();
    public List<AudioClip> sound = new List<AudioClip>();
    public AudioSource soundManager;
    [SerializeField]  private bool end;
    [SerializeField] private float timer = 2;
    public int numberSound;

    public bool endSound;

    public bool test2;
   

    // Update is called once per frame
    void Update()
    {
        if (test2)
        {
            Phase(numberSound);
        }

        if (endSound)
        {
            //UseSound(numberSound);
        }
        
       
    }

    public void Phase(int soundNumber)
    {
        if (soundManager.isPlaying == false)
        {
            soundManager.clip = voice[soundNumber];
            soundManager.Play();
        }
    }

    public void UseSound(int soundNumber)
    {
        if (soundManager.isPlaying == false)
        {
            soundManager.clip = sound[soundNumber];
            soundManager.Play();
        }
    }
 
}
