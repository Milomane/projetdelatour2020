﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;

    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioClip musicMenu;
    [SerializeField] private AudioClip musicMain;
    [SerializeField] private AudioClip musicActual;
    
    private int actualMusic = -1;

    public LevelManager levelManager;
    
    
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    
    void Update()
    {
        if (levelManager == null)
        {
            levelManager = LevelManager.instance;
            if (actualMusic != levelManager.musicNum)
            {
                ChangeMusic(levelManager.musicNum);
            }
        }
    }

    public void ChangeMusic(int musicToPlay)
    {
        actualMusic = musicToPlay;
        StartCoroutine(SmoothChange(musicToPlay));
    }

    public IEnumerator SmoothChange(int musicToPlay)
    {
        Debug.Log("Changing music : " + musicToPlay);
        float vol = 1;
        
        while (vol > 0)
        {
            vol -= 1 * Time.deltaTime;
            musicSource.volume = vol;
            yield return null;
        }
        vol = 0;
        
        musicSource.Stop();
        switch (musicToPlay)
        {
            case 0:
                musicSource.clip = musicMenu;
                break;
            case 1:
                musicSource.clip = musicMain;
                break;
            case 2:
                musicSource.clip = musicActual;
                break;
        }
        musicSource.Play();
        
        while (vol < 1)
        {
            vol += 1 * Time.deltaTime;
            musicSource.volume = vol;
            yield return null;
        }
        vol = 1;
    }
}
